import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public randomNumber = 0;

  ngOnInit() {
    // Generate random number between 1 & 100.
    this.randomNumber = Math.floor(Math.random() * 100) + 1;
  }
}
