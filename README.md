# Test prerender in angular 9
Will create project in a new docker container named "my-test-app".

## Prepare container with angular 9
1. `git clone git@gitlab.com:tzsveny/test-prerender.git`
2. `cd test-prerender`
3. `make up`
4. `make npm-install`

## Test prerender
1. `make prerender` ("npm run prerender")
2. `make serve-ssr` ("npm run serve:ssr")
3. Check localhost:4000 in browser with deactivated javascript
