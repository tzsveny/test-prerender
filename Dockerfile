FROM node:latest

ARG node_path_absolute
ARG app_name

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install vim -y

WORKDIR ${node_path_absolute}

# Set user "node" as owner.
USER root
RUN chown node:node ${node_path_absolute}
USER node

RUN mkdir ${node_path_absolute}/.npm-global
RUN mkdir ${node_path_absolute}/${app_name}

USER root

# Install angular cli.
RUN npm install -g @angular/cli --unsafe-permsdock

USER node

COPY ./app/* ${node_path_absolute}/${app_name}/

# Create Volume.
VOLUME ${node_path_absolute}/${app_name}
