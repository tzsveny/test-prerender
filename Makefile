# import config.
config ?= .env
include $(config)

up:
	docker-compose up --build -d

new-project:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE) && ng new ${APP_NAME}'

add-universal:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && ng add @nguniversal/express-engine'

## Stop & remove container.
remove:
	docker-compose down

## Stop container.
stop:
	docker-compose stop

## Continue stopped container.
start:
	docker-compose start

npm-install:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && npm install'

## Generates a component. (ng generate component pages/index/index --flat   --> to create folder)
component:
	test $(component)
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && ng generate component $(component)'

## Generates a service.
service:
	test $(service)
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && ng generate service $(service)'

## Serve the application.
## Builds the app, starts the development server, watches source files and rebuilds app as you make changes to those files.
serve:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && ng serve --host 0.0.0.0 --poll=2000 --project=$(APP_NAME)'

dev-ssr:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && npm run dev:ssr'

serve-ssr:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && npm run serve:ssr'

build-ssr:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && npm run build:ssr'

prerender:
	docker-compose exec -u node web sh -c 'cd $(NODE_PATH_ABSOLUTE)/$(APP_NAME) && npm run prerender'
